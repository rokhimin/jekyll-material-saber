<center><img src="https://github.com/rokhimin/jekyll-material-saber/blob/master/bg/material-saber.jpg" ></img></center>

# Material Saber
## About
Material saber jekyll inspired material theme.

###### Live
- host heroku : https://material-saber.herokuapp.com
- host netlify : https://material-saber.netlify.com
## Deploy

###### Heroku
[![Deploy to heroku](https://www.herokucdn.com/deploy/button.png)](https://dashboard.heroku.com/new?button-url=https://github.com/rokhimin/jekyll-material-saber&template=https://github.com/rokhimin/jekyll-material-saber)

###### Netlify
 [![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://github.com/rokhimin/jekyll-material-saber)

## Test Locally
- ``bundle install``
- ``bundle exec jekyll s``

## License
MIT License.
